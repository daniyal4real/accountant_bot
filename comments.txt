Скачайте SQLiteStudio

создайте базу данных в папке проекта, можно назвать accountant.db

таблицы: users(id, user_id, join_date)


CREATE TABLE user (
    id                 INTEGER PRIMARY KEY AUTOINCREMENT,
    user_id            INTEGER NOT NULL
                               UNIQUE,
    join_date_DATETIME         NOT NULL
                               DEFAULT ( (DATETIME('now') ) )
);



CREATE TABLE records (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    users_id INTEGER NOT NULL
                     REFERENCES user (id) ON DELETE CASCADE,
    operation BOOLEAN NOT NULL,
    value DECIMAL NOT NULL,
    date DATETIME NOT NULL
    DEFAULT ((DATETIME('now')))
);