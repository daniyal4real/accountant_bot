import sqlite3


try:
    conn = sqlite3.connect("accountant.db")
    cursor = conn.cursor()

    cursor.execute("INSERT OR IGNORE INTO `user` (`user_id`) VALUES (?)", (1000,))

    user = cursor.execute("SELECT * FROM `user`")

    conn.commit()

except sqlite3.Error as err:
    print("Ошибка: ", err)

finally:
    if(conn):
        conn.close()